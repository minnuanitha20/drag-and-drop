import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CdkDragDrop, CdkDragMove, CdkDragStart, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  menus = [
    { name: 'near_me' },
    { name: 'push_pin' },
    { name: 'desktop_windows' },
    { name: 'language' },
    { name: 'factory' },
    { name: 'horizontal_split' },
    { name: 'house' },
    { name: 'warehouse' },
    { name: 'map' },
    { name: 'local_shipping' },
    { name: 'meeting_room' }
  ]
  fields: any[] = [];
  @ViewChild('dropZone', { read: ElementRef, static: true }) dropZone!: ElementRef;
  @ViewChild('sidenav', { read: ElementRef, static: true }) sidenav!: ElementRef;
  _pointerPosition:any;
  _currentIndex:any;
  _currentField:any;
  constructor() { }

  ngOnInit(): void {
  }
  moved(event: CdkDragMove) {
    console.log(event, 'moved')
    this._pointerPosition=event.pointerPosition;
  }
  itemDropped(event: CdkDragDrop<any[]>) {
    console.log(event)
    if (event.previousContainer === event.container) {
      console.log(event, 'event')
      event.item.data.top=(this._pointerPosition.y-this.sidenav.nativeElement.getBoundingClientRect().top)+'px'
      event.item.data.left=(this._pointerPosition.x-this.sidenav.nativeElement.getBoundingClientRect().left)+'px'
      this.addField({...event.item.data}, event.currentIndex);
      // moveItemInArray(this.fields, event.previousIndex, event.currentIndex);
    } else {
      event.item.data.top=(this._pointerPosition.y-this.dropZone.nativeElement.getBoundingClientRect().top)+'px'
      event.item.data.left=(this._pointerPosition.x-this.dropZone.nativeElement.getBoundingClientRect().left)+'px'
      this.addField({...event.item.data}, event.currentIndex);
    }
  }

  addField(fieldType: string, index: number) {
    this.fields.splice(index, 0, fieldType)
  }

}
