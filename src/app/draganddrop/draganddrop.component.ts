import { Component} from '@angular/core';
@Component({
  selector: 'app-draganddrop',
  templateUrl: './draganddrop.component.html',
  styleUrls: ['./draganddrop.component.css']
})
export class DraganddropComponent {

  types = [
    {text:'square'},
    {text:'circle'},
    {text:'rectangle'},
    {text:'triangle-up'},

  ]
  itemDropped(event:any){
    console.log(event, 'event')
  }
}